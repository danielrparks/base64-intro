# base64 encoding intro
You might already be familiar with the trick that lets you convert easily between hex, binary, and octal. If you aren't, here's how it works:

Every hex digit reprents a number from `0x0` to `0xa`, or 0 to 15, for 16 total digits. You can also represent numbers from 0 to 15 with 4 binary digits:
```
0b0000 = 0

0b1111 = 15
  │││└ ones digit
  ││└─ twos digit
  │└─ fours digit
  └─ eights digit
  (1*1 + 1*2 + 1*4 + 1*8 = 15)
```

The same is true for octal, in which each digit reprents a number from 0 to 7:
```
0b000 = 0
0b111 = 7
```

You might have noticed that the number of bits used per digit is $$log_2 b$$, where $$b$$ is the base. This is the reason there is no such trick for decimal; $$log_2 10$$ is irrational. If it were rational, the denominator would be the number of decimal digits for which the maximum value would also be the maximum value of <numerator> binary digits, so the fact that it is not rational means there is no such number.

## What if we have a bigger base?
We can use this property to generate a system for representing binary data that doesn't contain any special characters, which is useful for allowing systems that assign special meanings to some ASCII control characters to communicate.

As you might have guessed, it's called base64 because the base is 64.
The digits are, in order,

```
ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/
```

And yes, unfortunately, 0_base64 is not 0_10.

Each digit in base64 represents _6_ bits in binary, which means that every 4 base64 digits represent 3 bytes (you can call "bytes" "base-256" digits, if you like).

## 4/3 is kinda weird
This leads to a problem when encoding bytes of data using base64: if the number of bytes in the data is not a multiple of 3, base64 will represent it unevenly.

| Data Type      | Data                                               |
| -------------- | -------------------------------------------------- |
| Bytes          | `    h         e         l         l         o   ` |
| Bits           | `01101000  01100101  01101100  01101100  01101111` |
| 6-grouped bits | `011010  000110 010101 101100  011011 000110 1111` |
| base64         | `   a       G      V      s        b     G   ????` |

We can fix this problem by adding one more character called _padding_. One or two `=`s at the end of a base64 message indicate that one or two byte should be ommitted from the output. So our `hello` example becomes

| Data Type      | Data                                               |
| -------------- | -------------------------------------------------- |
| Bytes          | `    h         e         l         l         o   ` |
| Bits           | `01101000  01100101  01101100  01101100  01101111` |
| 6-grouped bits | `011010  000110 010101 101100  011011 000110 1111` |
| base64         | `   a       G      V      s        b     G     8=` |

`8_base64` = `0b111100`, so we would have an extra two bits at the end of the message that should have been part of the 6th byte. The `=` indicates that the 6th byte should be dropped. The padding also ensures that the base64 message is a multiple of 4 characters.

Note that it would be meaningless to have 3 `=`s at the end of the message, because you could just omit the last 4 characters of the message entirely.

## Examples

- [hello world](helloworld.md)
- [image](image.md)
