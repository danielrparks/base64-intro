# Hello world in base64
The `base64` command is included in the `coreutils` package on Linux, which is probably already installed. You can use it to encode and decode base64 on the command line. See `man base64` for syntax.

In some implementations of base64, you can omit the `=`, because it's relatively easy to infer the length of the binary data. The Linux command requires it, but can still decode data without it (it just gives you an error).

Here's "hello world" encoded in base64:
`aGVsbG8gd29ybGQ`

Note that I have omitted the `=`. If you try to decode this with the `base64` command, you will get the error I mentioned above.

## Ideas for things to try
- Try adding padding so that it decodes correctly.
- What happens if you change the last character before the `=` to a different digit that has the same upper four bits?
